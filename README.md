# About the project

**Tomato Farm** is a simple habit tracker. It tracks several task categories (currently 12), configured in the script. I count the number of 20-min pomodoros for each task category, and the total is displayed in the center. The goal is to get to 1000. 

(It can be any number of items, not necessarily 12. But if there's more, or if images are bigger, they can overlap; I'll ignore this problem for now.)

It's not a very user-friendly system: not web-based, requires to update the numbers manually inside the script (the total, of course, is calculated automatically), and generates the output image locally. I keep it on my Dropbox.

All the images are from Flaticon, see resources.txt for the attribution, I hope I'm not forgetting any!


