#!/usr/bin/python3
import os, sys, math
from PIL import Image, ImageDraw, ImageFont

habits = [
    ("game_programming",1),
    ("game_writing",0),
    ("game_art",1),
    ("programming_misc",2),


    ("learning_comp",6),
    ("learning_lang",4),
    ('learning_misc',0),
    ('writing_misc',0),

    ('fitness',2),
    ('housework',3),
    ('reading',5),
    ('other',0)
]
num_habits = len(habits)

# size of habit icon
w = 64
h = 64

font = ImageFont.truetype('Oswald-Bold.ttf', 20)     #font for numbers
tfont = ImageFont.truetype('Oswald-Bold.ttf', 40)    #font for the total number

bgcolor = (60,60,120)
background = Image.new(mode="RGBA",size=(600,600), color=bgcolor)
tomato = Image.open("tomato.png").convert("RGBA")
twidth, theight = tomato.size

borders = []
numbers = []
cr = 50   #radius of border circle
cr1 = 25  #radius of number circle
tcolor = (0,40,0)   #text color

total = 0   #total number of poms for all habits
for i in range(num_habits):
    circle = Image.new(mode="RGBA",size=(cr*2,cr*2), color=bgcolor)
    c = ImageDraw.Draw(circle)
    c.ellipse((0, 0, cr*2-2, cr*2-2), fill = (200,200,255), outline ='blue')
    borders.append(circle)

    circle1 = Image.new(mode="RGBA",size=(cr1*2,cr1*2), color=bgcolor)
    c1 = ImageDraw.Draw(circle1)
    c1.ellipse((0, 0, cr1*2-2, cr1*2-2), fill = (230,230,230), outline =(50,0,0))

    (_,cnt) = habits[i]
    total += cnt
    text = str(cnt)
    wt, ht = c1.textsize(text, font=font)
    x = (cr1*2-wt)/2-2
    y = (cr1*2-ht)/2-2
    c1.text((x,y), text, fill=tcolor,font=font)
    numbers.append(circle1)

t = ImageDraw.Draw(tomato)
text = str(total)
wt, ht = t.textsize(text, font=tfont)
x = (twidth-wt)/2
y = int(theight*3/4-ht)
t.text((x,y), text, fill=tcolor,font=tfont)

images = [Image.open(i+".png").convert("RGBA") for (i,_) in habits]

r = 240    # radius of the habits circle
r1 = 150   # radius of the numbers circle
xc = 300   # center
yc = 300

step = int(360/num_habits)
for i in range(num_habits):
    degree = i*step-90 # start from the top center of the "clock"
    rad = (degree*math.pi)/180
    x = int(xc + r*math.cos(rad))
    y = int(yc + r*math.sin(rad))
    background.paste(borders[i], (x-cr,y-cr))
    background.paste(images[i], (int(x-w/2),int(y-w/2)), mask=images[i])

    x1 = int(xc + r1*math.cos(rad))
    y1 = int(yc + r1*math.sin(rad))
    background.paste(numbers[i], (x1-cr1,y1-cr1), mask=numbers[i])

background.paste(tomato,(int(xc-twidth/2),int(yc-theight/2)),mask=tomato)

background.save("graph.png")
